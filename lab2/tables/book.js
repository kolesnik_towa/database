class Book{
    constructor(id,name,num_of_pages, published, author, genre){
        this.id = id;
        this.name = name;
        this.num_of_pages = num_of_pages; 
        this.published = published; 
        this.author = author;
        this.genre = genre;
    }
    // constructor(id,name,num_of_pages, published, author_id){
    //     this.id = id;
    //     this.name = name;
    //     this.num_of_pages = num_of_pages; 
    //     this.published = published; 
    //     this.author_id = author_id;
    // }
}
module.exports = Book;