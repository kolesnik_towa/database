const DB = require("./db-class");
const Book = require("../tables/book");

class queries{
    constructor(dbConfig){
        this.db = new DB(dbConfig);
    }

    async getBooks(){
        const sql = "SELECT book.id, book.name, book.number_of_pages, book.published, author.name as author, " +
        "ARRAY( "+
            "SELECT genre.name "+
           " FROM genre "+
             "INNER JOIN book_genre ON book_genre.genre_id = genre.id "+
            "WHERE book.id = book_genre.book_id "+
            ")as genres "+
        "FROM book " +
        "INNER JOIN author ON author.id = book.author_id "+
        "INNER JOIN book_genre on book_genre.book_id = book.id "+
        "INNER JOIN genre ON genre.id = book_genre.genre_id "+
        "GROUP BY book.id, author.name ORDER BY book.id ASC";
        const result = await this.db.query(sql);
        return await books_parse(result);
    }
    async getBook(id){
        const sql = "SELECT book.id, book.name, book.number_of_pages, book.published, author.name as author, " +
        "ARRAY( "+
            "SELECT genre.name "+
           " FROM genre "+
             "INNER JOIN book_genre ON book_genre.genre_id = genre.id "+
            "WHERE book.id = book_genre.book_id "+
            ")as genres "+
        "FROM book " +
        "INNER JOIN author ON author.id = book.author_id "+
        "INNER JOIN book_genre on book_genre.book_id = book.id "+
        "INNER JOIN genre ON genre.id = book_genre.genre_id  WHERE "+
         `book.id=${id}` + " GROUP BY book.id, author.name";
        const result = await this.db.query(sql);
        return await books_parse(result);
    }
    async insertBook(book){
        if(!await this.is_book_exist(book.name))
        {
            const author = await this.is_author_exist(book.author);
            const genre = await this.is_genre_exist(book.genre);
            if(!author)
            {
                console.log("pricol1");
                return;
            }
            if(!genre)
            {
                console.log("pricol2");
                return;
            }
            let sql = `INSERT INTO book(name, number_of_pages, published, author_id) VALUES ('${book.name}',${book.num_of_pages}, '${book.published}' ,${author}) RETURNING id`;
            let result = await this.db.query(sql);
            sql = `INSERT INTO book_genre(book_id, genre_id) VALUES (${result.rows[0].id}, ${genre})`;
            result = await this.db.query(sql);
            return result.rows[0].id;
        }
        else{
            console.log("Exist");
        }
       
    }
    async updateBook(input){
        let result = null;
        let book = null
            if(!await this.is_book_exist(input[0])){
                console.log("))))))))")
                return 0;
            }
            else{
                let sql = `SELECT * FROM book WHERE name = '${input[0]}'`;
                book = await this.db.query(sql); 
            }
            const author = await this.is_author_exist(input[1].author);
            const genre = await this.is_genre_exist(input[1].genre);
            if(!author)
            {
                console.log("pricol1");
                return;
            }
            if(!genre)
            {
                console.log("pricol2");
                return;
            }
        let sql = `UPDATE public.book SET name='${input[1].name}', number_of_pages=${input[1].num_of_pages}, published='${input[1].published}',author_id=${author} WHERE id=${book.rows[0].id};`
        result = await this.db.query(sql); 

        sql = `INSERT INTO book_genre(book_id, genre_id) VALUES (${book.rows[0].id}, ${genre})`;
        result = await this.db.query(sql);
    }

    async removeBook(book) {
        const sql = `DELETE FROM book WHERE id = '${book}'`;
        const result = await this.db.query(sql);

        return result.rowCount;
    }
    async generationBooks(input){
        for(let i = 0; i < input; i++){
            let tempSql1 = await CharGeneration(await getRandomInt(10,15));
            let tempRes1 = await this.db.query(tempSql1);
            if(!await this.is_book_exist(tempRes1.rows[0].r))
            {
                let tempSql = await CharGeneration(await getRandomInt(10,15));
                let tempRes = await this.db.query(tempSql);
                const author = await this.is_author_exist(tempRes.rows[0].r);
                // console.log(tempRes.rows[0].r);
                tempSql = await CharGeneration(await getRandomInt(10,15));
                tempRes = await this.db.query(tempSql);
                const genre = await this.is_genre_exist(tempRes.rows[0].r);
                // console.log(tempRes.rows[0].r);
                
                sql = `INSERT INTO book(name, number_of_pages, published, author_id) VALUES ('${tempRes1.rows[0].r}',${IntGeneration(100,400,1)} , CAST((SELECT CAST(${IntGeneration(1900,2020,1)} as varchar)|| '-' || CAST(${IntGeneration(1,12,1)} as varchar)|| '-' || CAST(${IntGeneration(1,20,1)} as varchar)) as date), ${author}) RETURNING id`;
               
                let result = await this.db.query(sql);
                sql = `INSERT INTO book_genre(book_id, genre_id) VALUES (${result.rows[0].id}, ${genre})`;
                
                result = await this.db.query(sql);
            }
            else{
                console.log("Exist");
            }
            // process.stdout.write("\r\x1b[K");
            // process.stdout.write("[");
            // for(let j = 0; j < i; j+=input/100)
            // {
            //     process.stdout.write("=");
            // }
            // for(let j = 0; j < input - i - 1; j+=input/100)
            // {
            //     process.stdout.write(' ');
            // }
            // process.stdout.write("]");
            
            
        }
    }

    async searchBooks(input){
        let sql = "SELECT book.id, book.name, book.number_of_pages, book.published, author.name as author, " +
        "ARRAY( "+
            "SELECT genre.name "+
           " FROM genre "+
             "INNER JOIN book_genre ON book_genre.genre_id = genre.id "+
            "WHERE book.id = book_genre.book_id "+
            ")as genres "+
        "FROM book " +
        "INNER JOIN author ON author.id = book.author_id "+
        "INNER JOIN book_genre on book_genre.book_id = book.id "+
        "INNER JOIN genre ON genre.id = book_genre.genre_id ";
        var t = 0;
        if(input.id)
        {
            sql+=`  WHERE CAST(book.id as varchar) LIKE '%${input.id}%'`;
            t++;
        }
        if(input.name){
            if(t != 0)
            sql+=` AND `;
            else sql+=` WHERE `
            sql+=`LOWER(book.name) LIKE '%${input.name.toLowerCase()}%'`;
            t++;
        }
        
        if(input.num_of_pages){
            if(t != 0)
            sql+=` AND `;
            else sql+=` WHERE `
            sql+=`CAST(book.number_of_pages as varchar) LIKE  '%${input.num_of_pages}%'`;
            t++;
        }
        if(input.published){
            if(t != 0)
            sql+=` AND `;
            else sql+=` WHERE `
            sql+=`CAST(book.published as varchar) LIKE '%${input.published}%'`;
            t++;
        }
        
        if(input.author){
            if(t != 0)
            sql+=` AND `;
            else sql+=` WHERE `
            sql+=`LOWER(author.name) LIKE '%${input.author}%'`;
        
            t++;
        }
        if(input.genre){
            if(t != 0)
            sql+=` AND `;
            else sql+=` WHERE `
            sql+=`LOWER(genre.name) LIKE '%${input.genre}%'`;
        }
        sql += " GROUP BY book.id, author.name ";
        console.log(sql);
        const result = await this.db.query(sql);
        
        return [await books_parse(result), this.executionTime(sql)];
        
    }
    //
    // 
    async executionTime(query) {
        query = `EXPLAIN ANALYZE ${query}`;
        const res = await this.db.query(query);
        return res.rows[Object.keys(res.rows).length - 1]['QUERY PLAN'];
    }

    // 
    // 
    async is_book_exist(book){

        let sql = `SELECT * FROM book WHERE LOWER(name)='${book.toLowerCase()}'`;
        
        let result = await this.db.query(sql);
        if(result.rowCount === 0){
            return false;
        }
        return true;

    }
    async is_author_exist(author) {
        
        let sql = `SELECT * FROM author WHERE LOWER(name)='${author.toLowerCase()}'`;
        let result = await this.db.query(sql);
        if(result.rowCount === 0)
        {
            sql = `INSERT INTO author (name) VALUES ('${author}') RETURNING id`;
            result = await this.db.query(sql);
            if(result){
                return result.rows[0].id;
            }
        }
        else{
            return result.rows[0].id;
        }
        return false;
        
        
    }
    async is_genre_exist(genre) {
        let sql = `SELECT * FROM genre WHERE LOWER(name)='${genre.toLowerCase()}'`;
        let result = await this.db.query(sql);
        if(result.rowCount === 0)
        {
            sql = `INSERT INTO genre (name) VALUES ('${genre}') RETURNING id`;
            result = await this.db.query(sql);
            if(result){
                return result.rows[0].id;
            }
        }
        else{
            return result.rows[0].id;
        }
        return false;
    } 

}
async function getRandomInt(min, max) {
    return Math.floor(Math.random() * Math.floor(max-min)) + min + 1;
  }

function IntGeneration(min, max, count) {
    sql = `(SELECT trunc(random()*${max-min} + ${min})::int
    FROM generate_series(1,${count}))`
    
    return sql;
}

async function CharGeneration(count) {
    let sql;
    if(await getRandomInt(0,2) === 1)
    {
        sql = `(SELECT chr(trunc(random()*25 + 65)::int)`;
    }
    else{
        sql = `(SELECT chr(trunc(random()*25 + 97)::int)`;
    }
    
    for(let i = 0; i < count; i++)
    {
        if(await getRandomInt(0,2) === 1)
        {
            sql+=`|| chr(trunc(random()*25 + 65)::int)`
        }
        else{
            sql+=`|| chr(trunc(random()*25 + 97)::int)`
        }
        
    }
    sql += `as r) `
    
    return sql;
}

async function books_parse(data) {
    let books = [];
    if(data.rows.length){
        for(let temp of data.rows)
        {
            let book = new Book(
                temp.id,
                temp.name,
                temp.number_of_pages,
                temp.published.getFullYear() + "-" + temp.published.getDate() + "-" + temp.published.getMonth(),
                temp.author,
                temp.genres
            );
            
            books.push(book);
        }   
    }
    return books
}



module.exports = queries;