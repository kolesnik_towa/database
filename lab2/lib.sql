PGDMP                         x            library    11.10    11.10 $    -           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            .           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            /           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            0           1262    16393    library    DATABASE     �   CREATE DATABASE library WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
    DROP DATABASE library;
             postgres    false            �            1259    16394    author    TABLE     �   CREATE TABLE public.author (
    id integer NOT NULL,
    name text,
    nation text,
    data_of_birth text,
    death_data text
);
    DROP TABLE public.author;
       public         postgres    false            �            1259    16471    author_id_seq    SEQUENCE     �   ALTER TABLE public.author ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.author_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    196            �            1259    16515    book    TABLE     �   CREATE TABLE public.book (
    id integer NOT NULL,
    name text,
    number_of_pages integer,
    author_id integer,
    published date
);
    DROP TABLE public.book;
       public         postgres    false            �            1259    16513    book1_id_seq    SEQUENCE     �   CREATE SEQUENCE public.book1_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.book1_id_seq;
       public       postgres    false    203            1           0    0    book1_id_seq    SEQUENCE OWNED BY     <   ALTER SEQUENCE public.book1_id_seq OWNED BY public.book.id;
            public       postgres    false    202            �            1259    16532 
   book_genre    TABLE     g   CREATE TABLE public.book_genre (
    id integer NOT NULL,
    book_id integer,
    genre_id integer
);
    DROP TABLE public.book_genre;
       public         postgres    false            �            1259    16530    book_genre_id_seq    SEQUENCE     �   CREATE SEQUENCE public.book_genre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.book_genre_id_seq;
       public       postgres    false    205            2           0    0    book_genre_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.book_genre_id_seq OWNED BY public.book_genre.id;
            public       postgres    false    204            �            1259    16415    genre    TABLE     \   CREATE TABLE public.genre (
    id integer NOT NULL,
    name text,
    description text
);
    DROP TABLE public.genre;
       public         postgres    false            �            1259    16477    genre_id_seq    SEQUENCE     �   ALTER TABLE public.genre ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.genre_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    197            �            1259    16423    user    TABLE     X   CREATE TABLE public."user" (
    id integer NOT NULL,
    name text,
    nation text
);
    DROP TABLE public."user";
       public         postgres    false            �            1259    16479    user_id_seq    SEQUENCE     �   ALTER TABLE public."user" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public       postgres    false    198            �
           2604    16518    book id    DEFAULT     c   ALTER TABLE ONLY public.book ALTER COLUMN id SET DEFAULT nextval('public.book1_id_seq'::regclass);
 6   ALTER TABLE public.book ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    202    203            �
           2604    16535    book_genre id    DEFAULT     n   ALTER TABLE ONLY public.book_genre ALTER COLUMN id SET DEFAULT nextval('public.book_genre_id_seq'::regclass);
 <   ALTER TABLE public.book_genre ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    204    205    205            !          0    16394    author 
   TABLE DATA               M   COPY public.author (id, name, nation, data_of_birth, death_data) FROM stdin;
    public       postgres    false    196   x$       (          0    16515    book 
   TABLE DATA               O   COPY public.book (id, name, number_of_pages, author_id, published) FROM stdin;
    public       postgres    false    203   1       *          0    16532 
   book_genre 
   TABLE DATA               ;   COPY public.book_genre (id, book_id, genre_id) FROM stdin;
    public       postgres    false    205   L6       "          0    16415    genre 
   TABLE DATA               6   COPY public.genre (id, name, description) FROM stdin;
    public       postgres    false    197   u7       #          0    16423    user 
   TABLE DATA               2   COPY public."user" (id, name, nation) FROM stdin;
    public       postgres    false    198   �C       3           0    0    author_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.author_id_seq', 239, true);
            public       postgres    false    199            4           0    0    book1_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.book1_id_seq', 222, true);
            public       postgres    false    202            5           0    0    book_genre_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.book_genre_id_seq', 215, true);
            public       postgres    false    204            6           0    0    genre_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.genre_id_seq', 230, true);
            public       postgres    false    200            7           0    0    user_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.user_id_seq', 1, false);
            public       postgres    false    201            �
           2606    16401    author author_key 
   CONSTRAINT     O   ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_key PRIMARY KEY (id);
 ;   ALTER TABLE ONLY public.author DROP CONSTRAINT author_key;
       public         postgres    false    196            �
           2606    16537    book_genre b_g_key 
   CONSTRAINT     P   ALTER TABLE ONLY public.book_genre
    ADD CONSTRAINT b_g_key PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.book_genre DROP CONSTRAINT b_g_key;
       public         postgres    false    205            �
           2606    16523    book book_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.book DROP CONSTRAINT book_pkey;
       public         postgres    false    203            �
           2606    16422    genre genre_key 
   CONSTRAINT     M   ALTER TABLE ONLY public.genre
    ADD CONSTRAINT genre_key PRIMARY KEY (id);
 9   ALTER TABLE ONLY public.genre DROP CONSTRAINT genre_key;
       public         postgres    false    197            �
           2606    16430    user user_key 
   CONSTRAINT     M   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_key PRIMARY KEY (id);
 9   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_key;
       public         postgres    false    198            �
           2606    16524    book book_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_fkey FOREIGN KEY (author_id) REFERENCES public.author(id) ON UPDATE CASCADE ON DELETE CASCADE;
 8   ALTER TABLE ONLY public.book DROP CONSTRAINT book_fkey;
       public       postgres    false    203    196    2716            �
           2606    16538    book_genre book_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.book_genre
    ADD CONSTRAINT book_fkey FOREIGN KEY (book_id) REFERENCES public.book(id) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.book_genre DROP CONSTRAINT book_fkey;
       public       postgres    false    203    2722    205            �
           2606    16543    book_genre genre_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.book_genre
    ADD CONSTRAINT genre_fkey FOREIGN KEY (genre_id) REFERENCES public.genre(id) ON UPDATE CASCADE ON DELETE CASCADE;
 ?   ALTER TABLE ONLY public.book_genre DROP CONSTRAINT genre_fkey;
       public       postgres    false    205    2718    197            !   �  x�U�G�k9�@��U�
!/5���[�&z�{��ffu�� b<��C�}��������u��������Fn���?t��0z�3���7��/��wI�t^�����}��>�z�����޴�o0pA&��`��� ����0��e��Yd��9��<-7!�N��%3�<ϕ�-�3��)f�^(6�VT�f<�FB#����cq �9�u���Q����5�F���l͹�\P��~杻�ĝ��f��J���@7dmy�O�έ���;�����Ӯ��q}����0RF��t �(Ur��r�~[�*�a׎]�xl��������� ���鬝���56z?�R顉�X��:�B�.�i]��G���`W�͚+x�+�%�n(VƧh���P���;�cY��,�jwb��
-AL�V��l��H�T^`ۡ�e�r���S��
q�q{���:Y�Sny�p�D& �g�O�u
d�:)�g0�S�����أt
��b[�@�H�ư�dS�H�r'��cz��Ni=����3���L�VS�	r��@r'l,�BFɼ��ř
N�V.�	B��B~D�qٰ��� �Q�2a��m�R�m)?�y��(@j=~��Ƽ/���C!�~\P�
��X:v��� tE�$���aF���qCB��2��QS`z�;��~4���N�2�-�`��Y>�'bd{[��Rw �BVgG�|��������J��ାX�����A_��M���('�Þ?$#����v�����-g+�(� �"O�]�՝�9+G膨nХ�k	�Ƈ(N�;��u�|�Mi5>@4����C��j���x>���j�Q��R����/$8���dgJҵ �Q2�!�i�w2{�� n�ɞ���>����l�G�/]��/@�4�D2��Ք��+���NO�8����e]�}����NU �:ܖ�Y}�!RA��a�����I�� �^$S67:Fk"\U�
���BEI�h������(�Y�l��U$��
E�ܐ4�6�����zs[7~NE!�A~_���Q3��Tm��n|E��;Mw�v6�B��!���d��x����ڙ>]?H �����������D�lE�{�N�x��Mzk9��@K�Y�Qz�k��u-� ���A�RL�KG4'���aR7��䋯%�Q;��|.��~O�6b�(�@=�\�`�L�y�3��Р�|nH�K)���2�:�)���(�#oi�>�d�Vr��J�牼�d�''�.̄G I�5�2:�hf7S����y#��%�K�X���>�9�.f����)�P/??�tM�T��1���!��9�����VGH�(0������4*����6I&��=��*� ��A4���]�
{K~��*�df�t^���'�B9B�)�Z��j�N��`���u��ѴE_ޛ���__�A��b�}ہ;�`�p!�i�1>z��A���GEOy���CB\�Г<���R�n!�!��ֺV%�z�
�w$Plܚ�Qg�[����R3�_;b&h��Q{3tQ�&b����0��g�l��7ZzEMbjj)1�O,�V�VϦYQ�ZJ�X�Zc,S÷�/D�ʘ��:��^	�ډ�f)�kk��}iBzCzƻ��zmx�U�U�r�.�mӰ� ���vW��Q��&C�D���1�.PEn�`k��-އt�ꕃ�3���������E����z<(��A�q݅��p���åq�4���Ђ�dD\�<7���;Z�h�n�4g5	��-y��z�:����#�*�-���.���J/��Tu�\���#�`JQ�2�c
&׼6)��fU�V�0 $�bT}ޤUo�2��0 ����&���F�)j���fV���
d.���Ax%Q���"
I�� ��K�$Kh�o�8��N$6'Z��b��V	i̇��a��T\a�������f�-������j�h��<��!-do4��0���N����/�E+�Iv"���d.�Q��+���XŦ��-̓��� �L��L�܏��!�	/�xš���N�(#2���x2�b��sL6�����&qY���p��D�]N�!$���)�_�X� {��7U�Z�����&n��e�m�������Z�_�� ��T4��ԕ��-�Y>R�FhK�$7�3�����E\��=3�^g�.b��n�9.zKl���lgR�E����	7�謓��<.�R���El�:�A4e!��$ď�S�D!ݥ�˯�&)3dJ1�9��&�Y�)h���3��p�h�mIC3m�T�ӇX���uV�����(�Ά�/�1`B�M"��M�{nJ!z"P�q�Nf+y8a\�����U�Ij�-X��*��D�Z���q���H�����U��m�q���D�%<b�
3�CI�ڷn`�$b[�1��>M|۸�-]l%cX.��-��׬){d0Q���,����*F���O�Vc�-ٚޜ|�ן��d�)x���p�-b�i7U\������D�����$�[���y��D>�K5�3�Z7�8/����r��L��34q�G���a��������lR�Dm��jU��ԁ9G�V�la�ƭ%&�ZR�m.�$�8��k�/���:P�q�����eCfv�9�,�� ��|g5]7�����������nx(t��o�^��gB���fDCF�Fc���ArCҒ[�hQm��[���pZ5zn�@�@��;��Đ�;�UR\)�GuD/����s����Y���ɅuJKokp8��'Aⴃ�S[�T+�J�j\�ж�Զ��6��]��W�G�.Ô�ۈѺiE�?\+sd7�DN������q#����S�e:6�+�B1��Sݤd�'�ql
$Gz1���ey�{n���B�t�a�ĳQ�X�ύ�g���׷���^W��h��[7���+,D��~����7�ܕ5dW$O���K�k��H�ɔ)�>$H�пu�٢f�������8n�(uɍ�C.1YI��3�[`-���C&V�ġZ%}�]�dG���c�$�B463m&����FЕ�De�4s��Ӕ�m!��sOw��e~�*~%&���e�d�����q%.��#����'�D����W+9�xׄ쁢��=o[jr�<ѷ?{&?eW��-Ha:���xҖ���R�Z��Di�o�N_8�A��������>C�      (   #  x�E�I��J����U�KD�N�D�(3^��wfq�;Bw'2��̢�J��k��sۚ�e�	�
�%T��D�M(�����������3�3t"OA��������qZˣM���@�k|"��JT�I:��<��ux��)���P�4PQ��u���=z�e�]V�L��0��&����{�><�����a:UA}u]�R�����~��rخ��<rc2�@�3E��ɦd����ɰ�l�XJ�R�q͈Qg?M�e����
���:�Q���v_���kk9��d�^*����R?��p���|01 ױ�"��_��os�X7�_Q��HU���:��&��~��:�e7O�(�A�*L.���׼�� �ԫ���UB��4êP~TBg������Z�o~	XeD���tX��x�*c�?�>���h��D��WBg?]���}�Ί���B�z$�1lJ�錴��x�^K� )���C,_�X,}'����E���0;�j�6��go�bh03H��D3$��47�"|י_-�h���]Ҧ�M��u�og���AC��2���0;8�$��0���ZfFj"��w&��"Jڗ�Y~`���]}�9	�G���F��Ͻ
�*x惷�.���S�����m7\�ϫX�OFi�A�|������!�7U�[Z1�����#��þw�u�dq���M$p���9��o�N��r3�)��Nq~*~"`�"�I~7���^P��tM�$��q���mT^+�y���\��Fo��#X��ڥo�ku�99�	�A��=FLB�6�Ol��K��
"K~���d�Wh[������(���WN�Я65�s�گ��9&�"���J�V�Ym���û:5��v�ө.X��
i.��{Cv��.)��WG�?:�J%�o>x2^�	C������>���9��[�b�shĺ>6�*H��؉���!k��0!�<n�qp�κ/���0$����$���m�Ez���c���p1����i½�-#m+�ȢN�)��cY�H��wV�w��#���Ə�A&�M�sS��_�"�"�oz7q�ۗ�z=�����E(F��K��J��?ܷ���N�u;�O��2i�N!����̯�vс'Г���2�ܬ�{~�����\�d2��Tl��A*`lÌ_�8͟�a��֍O"��M㯓���w���a�cxU�c�Dcu~zw��V���� ��qq�E:�#�7t_��>�5R���aᣏP�1�|��s�����`�6>d���Ӂ�`&�1b�'���$I��[�      *     x�%�ѭ#1C��Q1K��^^�u�%�!D�`�H�~�;O����N>o����Ǟ|��f/>�?�W����;�ٷ�1���ݓ��:�θ�q���q�4�q�2��Vƕ�ʸv�q��yp��yp�WΐW�(��e\9��kG���S�݇��͎��aǢ��b����E7�,�y�`��;�9��vȉk���3���o\�[��:|��a'��v�[����u\G���p���V��[U�nn��nn��nn����V��.n��n�ps��Q�unS      "   R  x�-��r캮@�<�j�s%*���D9����&=�W��R��t���O��F.��?�`�h��lY�~ {�����	�S�4��g���/z���@L%5y1��_��Q�{Ջ�ŘAW����E�D(ڬ�Kv���/�v똳&Tg7̒���?����{'B�ӥ������4*�Ռ�� m�c1%�N���,�o�b�К&�0~ ����i�pd?O`^sP��uIL�lNo��o�iVy���0�����c��y�� �x�ܮ&1�5�K>�׸�ռ�F��?��`�qb�=�:��|'�.����W��=S{�+��AǛ�؏�98��w�Z]�5����'�rC���i�"*1{��ݜ�wj��7�96�9�h[B���fSUj����m���P�"�:��Ҩ�k�H��a4�fEb��? ?�8<ʝ[C'�k����F�Eo����&��`���L��1�9L �"�tZĕ�$>���t�C�����*c��<�6Õt�����9��i��0���� ��>��6�f�����h�m))�R��k�����Vb�T{��\�-�Tn�פ�݀�c������`)&�w�mR$U�	GV��b�N���&�b̞�>��dzutF��"���˴ꈜL]S��������8ř)�y|���^P�qu�]_�Nk�S��m�>��=M���R* ހ��A�<��j����(��%VY4�;��=�F��.���Όc�����q8�'hw!kJ���m���/��n�/��dVԸ����b!wF�/D[£����M���6<9J�0���dhU����j������x)��R���+x��"r'Ml�E��������j[=�E��ԵP�L�ΰ��|�R�#�w��'8�~���y�!������ja7^�)Gk��{������M�x�p����H�苂���Vѡs� x��t5���;�����zdK]�śSct��y�N�����&�����SG��&���lb8���6p������R+:�����~7ژ�;���ȿ�A�ڹKٝ����1�� m�U���i�
�/�\,�§w7�G��v%5J�[�Я�d� >?�=*~�ľ�������k�P�ӖEN��AE;%�����0�b�9]���IjLEs������0�ω�Dgs����)��Kb����7��J�P�!�cE��<'��.�%_p��_K��E�A���Q�;�ɹ1������Ɲv�ց�����Z֥,���{�jazk�n���}r��A*�ݶ��������@�y����a^\g�E�}�i�����`�O���Ц�6g:aGn{.�	�CO�i[���((U��&g���ǃ��EG�H�[������� 5�|��m#�UԻ:�h �%��|MW��*�� ;(�z��2�z,eAV�y�B�`�=��.1����'8LyS�v�B�$�.��������NեB���E������Q:��u�XB� �;oiS(W�H�?�_�b:&��]d�� ��k�U��U鰍�э|�:sUò���;P�d.���k���� �JY���������'(kI6�C��N��I#��踛�����7ؓ�f<i�x�}@bZ�c�ΐ�$ ��i&AT�����爁VS>_�$a�*��;�����BG|��n�і&-y*R���8�K��rZ$�?@�-�<p�|2U&�D��aȺ�l#d�KC���5�����:�%�yd��PS���)V�/���Xg��c�TG��<�/|�l�ZF���2���T08�Q$3$O@�!R�*Ŷ0����_�Hm�e5y���/F$A��r��y���� ~��~�&�ՇFj��j��3hKOfX��/ �ܑ@��s�bBߠ�Nk:-m��&��1H:Be�����h�¬�C֡�:kX�!�/�1�~�HŚ5~�UQ����v�{��Xn9�7Џ���q�M�%�~�Y�e���TO6�ǜ��k�yPs�R$ S��j|���q������7"�P$��Y��Z�Յ$���9�4PٺN)��"��!WP���}O��D��z]ձ�A�)Ho,�"��lj�qz
�j��)'ߤ�D���-춬�Iw�=��ЦtŲ�@�.�Td��4�Z�a%	6�����'��iס+$_HFV�p�fW�Kr� �Et�/</����8�άBt��L��4$�R�9ԋIVR%�CV5LZ�.���ȇҐn����55���!�fd��Fk`�&G(:���b���V��v��'��}�V���9�T��;>Rc*BRHE?キ:Va&u�\$�:^E�01����_�@�TL�bF83�	ɱ�)3DŚ��Vd#���٩Xu)�C�CF�K�:����9�
$$ݬZp�[�ֲ��d$��1?S�11�$$�vH��4	���M�)� ��ի_'��$?�KTڴ�<���!5����\}cWziN�tҒmE�St!�d�$��+^w5�6�~�>20K(у2}�)���%Vk�=��~$/BFR�GǬ�4�e��DBR��j<��Z�J�$���7����������x[����A�s�Tt��6�k�g��#�ɦ#N,i���&�B2�S�`qf�YV��5�e�#�d��Wf�w� )&���љ�BJ��l6�3�ɰG:�ly�;cɕ-�>]*]I�0C%hLRtHH\xJ*�j�
)I�ki�SIS/�Ő %�5��Ӯ$h�>��u�=w������$e5�6���w������EW8��녔@T�qS��b}�I��}t�����4Vi�&n�� ґg�e�b'q�3��~l�VN��l�J���AqU!�:����x�~�����}����T0e�(k��F�U�&�$(d$[T8]Ǻ�無a(�hB�&�'a7��_g����H.�1fװ���ꪪ��'�(W7���i
�c7d��Z��q�UV!�B��e�*�Kg�XSE�P<�İv��� ttt��k9�R��*���6a�ܨ@�҃��ڒ��'��!�Q҆Fh ]&�w0p��ݳz��	|�ɫ����2W������V�j<�^���F���}�J$W�:dss�朻^���c? �(���b�<��/(�	=�	����������?�{��      #      x�3�L,N�L)N����� "f     