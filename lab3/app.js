const Controller = require("./logic/controller");
const Router = require("./logic/router");
const Color = require("colors");
const Book = require("./tables/book");
const prompt = require('prompt-sync')();


const commands = {
    book_l: 'book -l',
    book_id: 'book -id',
    book_i: 'book -i',
    book_r: 'book -r',
    book_u: 'book -u',
    // book_s: 'book -s',
    // book_g: 'book -g',
    help: 'help',
    exit: 'exit'
};

const router = new Router();
router.use(commands.book_l, Controller.getBooks);
router.use(commands.book_id, Controller.getBook);
router.use(commands.book_i, Controller.insertBook);
router.use(commands.book_r, Controller.removeBook);
router.use(commands.book_u, Controller.updateBook);
// router.use(commands.book_s, Controller.searchBooks);
// router.use(commands.book_g, Controller.generationBooks);
router.use(commands.exit, Controller.exit);

async function RUN(){
    // await router.handle('book -l');
    
    // await router.handle('book -id', 11);
    // await router.handle('book -u', ['asddsae123132', b]);
    // await router.handle('book -l');
    // await router.handle('book -r', 'updatetest');
    // await router.handle('book -i', b)
    // await router.handle('book -l');
    // await router.handle('book -g', 20);
    // await router.handle('book -l');

    // let b = new Book(1, 'a', 1, '9',null,'far');
    // await router.handle('book -s', b);
    let run = true;
    console.log('You could enter "help" to see all of commands');
    while(run){
        var input = prompt('>');
        const parts = input.split(' ');
        const command = (parts.slice(0, 2)).join(' ');
        switch (command) {
            case commands.book_l:
                try{
                    await router.handle('book -l');
                }
                catch(e)
                {
                    console.log(e.message);
                }
                break;

            case commands.book_id:
                try{
                    await router.handle('book -id', parts[2]);
                }
                catch(e)
                {
                    console.log(e.message);
                }
                break;

            case commands.book_i:
                try{
                    await router.handle('book -i');
                }
                catch(e)
                {
                    console.log(e.message);
                }
                break;
                
            case commands.book_u:
                await router.handle('book -u');
                break;
                
            case commands.book_r:
                await router.handle('book -r', parts[2]);
                break;
                
            // case commands.book_s:
            //     await router.handle('book -s');
            //     break;
                
            // case commands.book_g:
            //     await router.handle('book -g', parts[2]);
            //     break;
                
            case commands.exit:
                await router.handle('exit');
                run = false;
                break;
            case commands.help:
                console.log("===========================================".yellow)
                console.log('commands:');
                console.log("book -l -- books list");
                console.log("book -id {id} -- output book with id {id}");
                console.log("book -i -- insert book");
                console.log("book -u -- update book");
                console.log("book -r {id} -- remove book with id {id}")
                // console.log("book -g {num} -- generate {num} book(s) ")  ;
                console.log("exit -- enable the program")
                console.log("===========================================".yellow)
                break;
        
            default:
                await router.handle('exit');
                run = false;
                break;
        }
    }
}

RUN();
// const Sequelize = require("sequelize");
// const sequelize = new Sequelize("lab2", "postgres", "7895123", {
//     dialect: "postgres",
//     host: "localhost",
//     port: "5432",
//     define: {
//         timestamps: false
//     },
//     logging: false

// });
// console.dir(sequelize);

// const tables = require('./database/db-tables');


// async function run() {
//     tables.books.findAll({ 
//         raw : true,
//         limit: 10
//     })
//         .then(book => {
//             if (!book) return;
//             console.log(book.name);
//         }).catch(err => console.log(err));
// }   
// run();
