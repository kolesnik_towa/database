class Book{
    constructor(id,name,num_of_pages, published, author, genre){
        this.id = id;
        this.name = name;
        this.num_of_pages = num_of_pages; 
        this.published = published; 
        this.author = author;
        // this.genre = genre;
    }
}
module.exports = Book;