const Sequelize = require("sequelize");


const sequelize = new Sequelize("lab2", "postgres", "7895123", {
    dialect: "postgres",
    host: "localhost",
    port: "5432",
    define: {
        timestamps: false
    },
    logging: false

});
module.exports = {

    
    sequelize : sequelize,
    books : sequelize.define("books", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        number_of_pages: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        author_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'authors',
                key: 'id'
            }
        },
        published: {
            type: Sequelize.DATE,
            allowNull: true
        }
    }),

    authors : sequelize.define("authors", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        nation: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        data_of_birth: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        death_data: {
            type: Sequelize.TEXT,
            allowNull: true
        }
    }),

    genres : sequelize.define("genres", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: true
        },
        description: {
            type: Sequelize.TEXT,
            allowNull: true
        }
    }),

    books_genres : sequelize.define("books_genres", {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        book_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'books',
                key: 'id'
            }
        },
        genre_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
            references: {
                model: 'genres',
                key: 'id'
            }
        }
    })
}



