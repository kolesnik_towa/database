
const Book = require("../tables/book");
const tables = require('./db-tables');

class queries{
    constructor(){

        tables.authors.hasMany(tables.books, {
            as: "books",
            foreignKey: 'author_id'
        });
        tables.books.belongsTo(tables.authors, {
            foreignKey: 'author_id',
            as: 'author'
        });
        tables.books.belongsToMany(tables.genres, {
            through: tables.books_genres,
            foreignKey: 'book_id'
        });
        tables.genres.belongsToMany(tables.books, {
            through: tables.books_genres,
            foreignKey: 'genre_id'
        });

        try {
            tables.sequelize.sync({
                force: false
            });
        } catch (error) {
            console.log("ERROR");
        }
    }

    async getBooks(){
        let books;
        try {
            books = await tables.authors.findAll({
                raw: true,
                where : {id : 45},
                include: ["books"]
            });
        }
        catch (err)
        {
            console.error(err);
        }

        return books_parse(books);
    }
    async getBook(id){
        let books;
        try {
            books = await tables.books.findAll({
                raw: true,
                where: {
                    id: id
                }
            });
        } catch (err) {
            console.error(err);
        }

        // console.dir(books);
        return books;
    }
    async insertBook(book) {
        var t = await tables.authors.findOne({
            where: {
                name:book.author
            }
        })
        if (t == null) {
            t = await tables.authors.create({
                name: book.author
            });
        }
        // console.log(res.dataValues);
        t = await tables.books.create({
            name: book.name,
            number_of_pages: book.num_of_pages,
            author_id: t.dataValues.id,
            published: book.published
        });
    }
    async updateBook(input) {
        var t = await tables.authors.findOne({
            where: {
                name: input[1].author
            }
        })
       if (t == null) {
           t = await tables.authors.create({
               name: input[1].author
           });
       }
    //    console.log(input[0]);
       try {
           t = await tables.books.update({
               name: input[1].name,
               number_of_pages: input[1].num_of_pages,
               author_id: t.dataValues.id,
               published: input[1].published
           }, {
                   where: {
                   name: input[0]
               }
           });
       } catch (err) {
           console.error(err);
       }
    }

    async removeBook(book) {
        try {
            var result = tables.books.destroy({
                where: {
                    id: book
                }
            })
        } catch (err)
        {
            console.error(err);
        }

        return result;
    }
}

async function books_parse(data) {
    let books = [];
    if(data.length){
        for(let temp of data)
        {
            let book = new Book(
                temp['books.id'],
                temp['books.name'],
                temp['books.number_of_pages'],
                temp['books.published'],
                temp.name
                
            );
            
            books.push(book);
        }   
    }
    return books
}



module.exports = queries;