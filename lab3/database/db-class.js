const { Pool } = require("pg");

class database {

    constructor(db_config){
        this.pool = null
        this.conf = db_config;
    }


    async connect() {
		try { this.pool = await new Pool(this.conf); } 
		catch (ex) { console.log(`Error connection.\n${ex.stack}`); }
    }
    async disconnected() {
		try { await this.pool.end(); } 
		catch (ex) { console.log(`Error disconnection.\n${ex.stack}`); }
    }
    

    async query(query){
        let result = null;
        
        try{
            await this.connect();
            result = await this.pool.query(query);
        }catch(err){
            await console.log(err.stack);
        }finally{
            await this.disconnected();
            return result;
        }

    }
}

module.exports = database;