const Table = require('as-table');
const Colors = require('colors');
const prompt = require('prompt-sync')();


const db = require("../database/db-queries");
// const dbConfig = require("../database/db-config");
const Book = require('../tables/book');
//в параметры должены идти проверенные данные
const database = new db();
module.exports = {
    async getBooks(){
        result = await database.getBooks();
        print(result);
    },
    async getBook(input){
        // 
        //data verification
        input = Number(input);
        if(input <= 0) throw new Error("id couldn`t be less then 1".red);
        if(!input) throw new Error("Yot enter incorrect id!".red);
        // 
        result = await database.getBook(input);
        print(result);
    },
    async insertBook(){
        //get data
        const input = new Book();
        input.name = prompt('Name: ');
        input.num_of_pages = Number(prompt('Number of pages: '));
         //data verification
         if(input.num_of_pages <= 0) throw new Error("It couldn`t be less then 1".red);
         if(!input.num_of_pages) throw new Error("Yot enter incorrect id!".red);
         // 
        input.published = prompt('Published(YYYY-MM-DD): ');
         //data verification
         var temp = input.published.split('-');
         if(temp.length < 3) throw new Error("Incorrect date!".red);
         if(temp[0].length < 4 
            || temp[0].length > 4 
            || temp[1].length < 1 
            || temp[1].length > 2 
            || temp[2].length < 1 
            || temp[2].length > 2) throw new Error("Incorrect date!".red);
         
         if(!Number(temp[0]) || !Number(temp[1]) || !Number(temp[2])) throw new Error("Incorrect date!".red);
         // 
        input.author = prompt('Author: ');
        input.genre = prompt('Genre: ');
        //  
        result = await database.insertBook(input);
    },
    async updateBook(){
        //get data
        const input = 
        [   
            null,
            new Book()
        ]
        input[0] = Number(prompt('Enter id: '));
         // 
        //data verification
        if(input <= 0) throw new Error("id couldn`t be less then 1".red);
        if(!input) throw new Error("Yot enter incorrect id!".red);
        // 
        
        var tempBook = await database.getBook(input[0]);
        if(tempBook.length == 0) {
            console.log("Does not exist!");
            return 0;
        }
        input[0] = tempBook[0].name;
        input[1].name = prompt('New name: ');
        if(!input[1].name) input[1].name = tempBook[0].name;
        input[1].num_of_pages = Number(prompt('New number of pages: '));
        if(!input[1].num_of_pages) input[1].num_of_pages = tempBook[0].num_of_pages;
         //data verification
         if(input[1].num_of_pages <= 0) throw new Error("It couldn`t be less then 1".red);
         // 
         
        input[1].published = prompt('New published(YYYY-MM-DD): ');
        if(!input[1].published) input[1].published = tempBook[0].published;
         //data verification
         var temp = input[1].published.split('-');
         if(temp.length < 3) throw new Error("Incorrect date!".red);
         if(temp[0].length < 4 
            || temp[0].length > 4 
            || temp[1].length < 1 
            || temp[1].length > 2 
            || temp[2].length < 1 
            || temp[2].length > 2) throw new Error("Incorrect date!".red);
         
         if(!Number(temp[0]) || !Number(temp[1]) || !Number(temp[2])) throw new Error("Incorrect date!".red);
         // 
        input[1].author = prompt('New author: ');
        if(!input[1].author) input[1].author = tempBook[0].author;
        input[1].genre = prompt('New genre: ');
        if(!input[1].genre) input[1].genre = tempBook[0].genre;
        //  
        result = await database.updateBook(input);
    },
    async removeBook(input){
         // 
        //data verification
        input = Number(input);
        if(input <= 0) throw new Error("id couldn`t be less then 1".red);
        if(!input) throw new Error("Yot enter incorrect id!".red);
        // 
        try {
            result = await database.removeBook(input);
        }
        catch(err) {
            console.error(err);
        }
        if(result)
        {
            console.log("Suc");
        }
        else{
            console.log("Doesnt ex");
        }

    },
    
    exit(){
        console.log("\nSee you!\n".green);
	    process.exit(0);
    }
}
// 
// 
// 
// 
const tableConfig = {
	delimiter: '  ▏ '.blue,
	dash: '⎯'.blue,
	left: true,
	title: x => x.white,
	print: x => (typeof x === 'number') ? String(x).blue : String(x)
};

function print(date) {
	if (date.length > 0) { console.log(`\n${Table.configure(tableConfig)(date)}\n`); }
	else { console.log('\nThere is nothing!\nTry again.\n'.red) }
}

// function printExplain(explain) {
// 	console.log(`\n=> ${explain.green}\n`.green);
// }